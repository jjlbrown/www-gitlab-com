This checklist is based on the [Inbound Marketing](https://about.gitlab.com/handbook/marketing/inbound-marketing/) team's documentation for engineering AB tests using feature flag systems in locations such as the /pricing/ page.

## Feature-flag-based A/B Testing

### Dev Environment

- [ ] A feature flag has been created.
- [ ] Appropriate experiment events have been attached to the flag.
- [ ] Your HAML or ERB partial config has been adjusted to use the dev environment.
- [ ] The control variant appears as expected.
- [ ] The test variant appears as expected.

### Test Environment

- [ ] Your partial config has been adjusted to use the test environment.
- [ ] The javascript SDK has been enabled in the LD test environment.
- [ ] The control variant has been reviewed in the LD test environment without using URL parameters on a review app before release.
- [ ] The test variant has been reviewed in the LD test environment without using URL parameters on a review app before release.
- [ ] All critical functionality has been reviewed.
- [ ] All of the buttons work as expected, outside and inside the popup.
- [ ] All of the classnames and IDs required for tracking are present.
- [ ] The feature flag is enabled in the LD test environment.
- [ ] The metrics are set to record in the LD test environment.
- [ ] The metrics are recording as expected in the LD test environment.
- [ ] The LD debugger has been used to verify that the metrics are being recorded.

### Production Environment

- [ ] Your partial config has been adjusted to use the production environment, committed, and pushed.
- [ ] You have merged the branch into master. Note that after the pipeline is deployed it will take additional time for the CDN to propogate.
- [ ] The control variant has been verified by using URL parameters on production after release.
- [ ] The test variant has been verified by using URL parameters on production after release.
- [ ] The feature flag is enabled in the LD production environment.
- [ ] The rollout rule is setup correctly in the LD production environment.
- [ ] The metrics are set to record in the LD production environment.
- [ ] The feature flag javascript SDK has been enabled. This is the final step and live experiment data will now be gathered.

### Monitoring Phase

- [ ] I have created and linked an issue for monitoring the results of the test, including frequent check-ins about how many monthly allocated MAU and metrics we're using, until statistical significance is gathered. For production pricing pages with a 50/50 rollout, this is likely at least 2 weeks.
- [ ] I will be closely watching to ensure that production metrics aren't significantly negatively impacted and might end the experiment early if that is the case.

### Closing Phase

- [ ] I have created and linked a retrospective issue and reserved time to document the results and what we learned from this experiment once it's over.
- [ ] I have disabled the feature flag in the LD production, test, and dev environments.
- [ ] I have disabled the metrics gathering for the feature flag in the LD production, test, and dev environments. This normally happens when disabling the feature flag.
- [ ] I have disabled the javascript SDK for the feature flag in the LD production, test, and dev environments. This will prevent phantom MAU accumulation.

### Known gotchas

- If you disable the feature flags but not the SDK, then that feature flags will no longer be served, BUT anyone with a browser tab open may continue to submit data. People have been known to submit experiment data weeks after the test has ended. This is why we recommend disabling the javascript sdk once the experiment has ended.
