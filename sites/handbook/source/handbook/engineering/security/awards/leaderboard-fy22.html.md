---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@cablett](https://gitlab.com/cablett) | 1 | 600 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 2 | 500 |
| [@engwan](https://gitlab.com/engwan) | 3 | 480 |
| [@whaber](https://gitlab.com/whaber) | 4 | 400 |
| [@alexpooley](https://gitlab.com/alexpooley) | 5 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 7 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 8 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 9 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@mksionek](https://gitlab.com/mksionek) | 14 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 16 | 140 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 18 | 100 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 19 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 20 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 21 | 100 |
| [@iamphill](https://gitlab.com/iamphill) | 22 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 23 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 24 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 26 | 80 |
| [@twk3](https://gitlab.com/twk3) | 27 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 32 | 80 |
| [@manojmj](https://gitlab.com/manojmj) | 33 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 35 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 36 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 37 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 38 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 39 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 40 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 41 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 42 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 43 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 44 | 40 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 45 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 46 | 30 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@cablett](https://gitlab.com/cablett) | 1 | 600 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 2 | 500 |
| [@engwan](https://gitlab.com/engwan) | 3 | 480 |
| [@whaber](https://gitlab.com/whaber) | 4 | 400 |
| [@alexpooley](https://gitlab.com/alexpooley) | 5 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 7 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 8 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 9 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@mksionek](https://gitlab.com/mksionek) | 14 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 16 | 140 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 18 | 100 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 19 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 20 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 21 | 100 |
| [@iamphill](https://gitlab.com/iamphill) | 22 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 23 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 24 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 26 | 80 |
| [@twk3](https://gitlab.com/twk3) | 27 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 32 | 80 |
| [@manojmj](https://gitlab.com/manojmj) | 33 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 35 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 36 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 37 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 38 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 39 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 40 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 41 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 42 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 43 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 44 | 40 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 45 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 46 | 30 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


