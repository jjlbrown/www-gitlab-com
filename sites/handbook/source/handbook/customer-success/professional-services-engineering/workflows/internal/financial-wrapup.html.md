---
layout: handbook-page-toc
title: Financial Wrap-up
category: Internal
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Financial Wrap-up
Once SOW items have been delivered and ready for billing and/or revenue recognition, the following should be completed:

## Training completion or entire project is complete:
1. Attach the signed Project sign off document or attach the email where the Project sign off document was sent to the customer via email in the PSE Object
1. Update the PSE Object dates and update the status to `Closure`
1. The Project Coordinator will update the PSE Object status to `Complete` once month end is completed and closed with the Finance team

## Milestone completion 
Milestone completion ready for billing and/or revenue recognition, the following should be completed:
1. Attach the signed Project sign off document or attach the email where the Project sign off document was sent to the customer via email in the PSE Object
1. Attach the Trainig Roster to the PSE Object when the training is complete
1. The Project Coordinator will then tag the billing and revenue team in the PSE Object to make Finance aware of completion

## Monthly T&M Hours 
Monthly T&M Hours ready for billing and or revenue recognition, the following should be completed:
1. Project Coordinator pulls the hourly tracking report and add the report to the PSE Object
1. Project Coordinator will then tag the billing and revenue team in the PSE Object to make Finance aware of completion

Projects that contain Milestone or T&M Hourly billing are completed in full the project status should be updated to `Closure` status and project dates updated.
The Project Coordinator will update the PSE Object status to `Complete` once month end is completed and closed with the Finance team.
